﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TimeСontrol
{
    public partial class Form1 : Form
    {
        enum Status { Break, CoffeBreak, PlayGame, Programming, InternetSurfing }
        Status status;

        DateTime timeStartEstimated;
        DateTime timeStatusEstimated;

        struct Timers
        {
            DateTime online, status;
            public DateTime Online
            {
                get { return online; }
                set { online = value; }
            }
            public DateTime Status
            {
                get { return status; }
                set { status = value ; }
            }
        }
        Timers timers;

        public Form1()
        {
            InitializeComponent();
            timeStartEstimated = DateTime.Now;
            timeStatusEstimated = timeStartEstimated;

            status = Status.Break;

            timers.Online = new DateTime(timeStartEstimated.Year, timeStartEstimated.Month, timeStartEstimated.Day, 0, 0, 0);
            timers.Status = timers.Online;
            Timing();
        }

        private void b_Click(object sender, EventArgs e)
        {
            timers.Status = new DateTime(timeStartEstimated.Year, timeStartEstimated.Month, timeStartEstimated.Day, 0, 0, 0);
        }

        void Timing()
        {
            System.Threading.Timer timer = new System.Threading.Timer(RefreshTimeOnLabel, timers, 0, 1000);
            bool b = true;
        }
        
        void RefreshTimeOnLabel(object state)
        {
            Timers tmpTimers = (Timers)state;
            tmpTimers.Online = tmpTimers.Online.AddSeconds(1);
            tmpTimers.Status = tmpTimers.Status.AddSeconds(1);
            Action action = () => { timers.Status = tmpTimers.Status; timers.Online = tmpTimers.Online; };
            if (InvokeRequired)
                Invoke(action);
            
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            l_StatusValue.Text = timers.Status.ToLongTimeString();
            l_OnlineTimeValue.Text = timers.Online.ToLongTimeString();
        }
    }
}
