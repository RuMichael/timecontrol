﻿namespace TimeСontrol
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label l_Status;
            System.Windows.Forms.Label l_Time;
            System.Windows.Forms.Label l_OnlineTime;
            this.b_Break = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.b_CoffeBreak = new System.Windows.Forms.Button();
            this.b_Game = new System.Windows.Forms.Button();
            this.b_Programming = new System.Windows.Forms.Button();
            this.b_InternetSurfing = new System.Windows.Forms.Button();
            this.l_OnlineTimeValue = new System.Windows.Forms.Label();
            this.l_TimeValue = new System.Windows.Forms.Label();
            this.l_StatusValue = new System.Windows.Forms.Label();
            l_Status = new System.Windows.Forms.Label();
            l_Time = new System.Windows.Forms.Label();
            l_OnlineTime = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // l_Status
            // 
            l_Status.AutoSize = true;
            l_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            l_Status.Location = new System.Drawing.Point(458, 27);
            l_Status.Name = "l_Status";
            l_Status.Size = new System.Drawing.Size(60, 16);
            l_Status.TabIndex = 6;
            l_Status.Text = "Статус: ";
            // 
            // l_Time
            // 
            l_Time.AutoSize = true;
            l_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            l_Time.Location = new System.Drawing.Point(605, 27);
            l_Time.Name = "l_Time";
            l_Time.Size = new System.Drawing.Size(55, 16);
            l_Time.TabIndex = 7;
            l_Time.Text = "Время: ";
            // 
            // l_OnlineTime
            // 
            l_OnlineTime.AutoSize = true;
            l_OnlineTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            l_OnlineTime.Location = new System.Drawing.Point(605, 65);
            l_OnlineTime.Name = "l_OnlineTime";
            l_OnlineTime.Size = new System.Drawing.Size(99, 16);
            l_OnlineTime.TabIndex = 8;
            l_OnlineTime.Text = "Время в сети: ";
            // 
            // b_Break
            // 
            this.b_Break.Location = new System.Drawing.Point(12, 27);
            this.b_Break.Name = "b_Break";
            this.b_Break.Size = new System.Drawing.Size(69, 54);
            this.b_Break.TabIndex = 0;
            this.b_Break.Text = "Перерыв";
            this.b_Break.UseVisualStyleBackColor = true;
            this.b_Break.Click += new System.EventHandler(this.b_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(840, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // b_CoffeBreak
            // 
            this.b_CoffeBreak.Location = new System.Drawing.Point(212, 27);
            this.b_CoffeBreak.Name = "b_CoffeBreak";
            this.b_CoffeBreak.Size = new System.Drawing.Size(69, 54);
            this.b_CoffeBreak.TabIndex = 2;
            this.b_CoffeBreak.Text = "Чай/Кофе";
            this.b_CoffeBreak.UseVisualStyleBackColor = true;
            this.b_CoffeBreak.Click += new System.EventHandler(this.b_Click);
            // 
            // b_Game
            // 
            this.b_Game.Location = new System.Drawing.Point(287, 27);
            this.b_Game.Name = "b_Game";
            this.b_Game.Size = new System.Drawing.Size(69, 54);
            this.b_Game.TabIndex = 3;
            this.b_Game.Text = "Игры";
            this.b_Game.UseVisualStyleBackColor = true;
            this.b_Game.Click += new System.EventHandler(this.b_Click);
            // 
            // b_Programming
            // 
            this.b_Programming.Location = new System.Drawing.Point(87, 27);
            this.b_Programming.Name = "b_Programming";
            this.b_Programming.Size = new System.Drawing.Size(119, 54);
            this.b_Programming.TabIndex = 4;
            this.b_Programming.Text = "Программирование";
            this.b_Programming.UseVisualStyleBackColor = true;
            this.b_Programming.Click += new System.EventHandler(this.b_Click);
            // 
            // b_InternetSurfing
            // 
            this.b_InternetSurfing.Location = new System.Drawing.Point(362, 27);
            this.b_InternetSurfing.Name = "b_InternetSurfing";
            this.b_InternetSurfing.Size = new System.Drawing.Size(69, 54);
            this.b_InternetSurfing.TabIndex = 5;
            this.b_InternetSurfing.Text = "Интернет";
            this.b_InternetSurfing.UseVisualStyleBackColor = true;
            this.b_InternetSurfing.Click += new System.EventHandler(this.b_Click);
            // 
            // l_OnlineTimeValue
            // 
            this.l_OnlineTimeValue.AutoSize = true;
            this.l_OnlineTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_OnlineTimeValue.Location = new System.Drawing.Point(710, 65);
            this.l_OnlineTimeValue.Name = "l_OnlineTimeValue";
            this.l_OnlineTimeValue.Size = new System.Drawing.Size(0, 16);
            this.l_OnlineTimeValue.TabIndex = 11;
            // 
            // l_TimeValue
            // 
            this.l_TimeValue.AutoSize = true;
            this.l_TimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_TimeValue.Location = new System.Drawing.Point(710, 27);
            this.l_TimeValue.Name = "l_TimeValue";
            this.l_TimeValue.Size = new System.Drawing.Size(0, 16);
            this.l_TimeValue.TabIndex = 10;
            // 
            // l_StatusValue
            // 
            this.l_StatusValue.AutoSize = true;
            this.l_StatusValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_StatusValue.Location = new System.Drawing.Point(458, 65);
            this.l_StatusValue.Name = "l_StatusValue";
            this.l_StatusValue.Size = new System.Drawing.Size(0, 16);
            this.l_StatusValue.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 93);
            this.ControlBox = false;
            this.Controls.Add(this.l_OnlineTimeValue);
            this.Controls.Add(this.l_TimeValue);
            this.Controls.Add(this.l_StatusValue);
            this.Controls.Add(l_OnlineTime);
            this.Controls.Add(l_Time);
            this.Controls.Add(l_Status);
            this.Controls.Add(this.b_InternetSurfing);
            this.Controls.Add(this.b_Programming);
            this.Controls.Add(this.b_Game);
            this.Controls.Add(this.b_CoffeBreak);
            this.Controls.Add(this.b_Break);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Time Control";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_Break;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.Button b_CoffeBreak;
        private System.Windows.Forms.Button b_Game;
        private System.Windows.Forms.Button b_Programming;
        private System.Windows.Forms.Button b_InternetSurfing;
        private System.Windows.Forms.Label l_OnlineTimeValue;
        private System.Windows.Forms.Label l_TimeValue;
        private System.Windows.Forms.Label l_StatusValue;
    }
}

